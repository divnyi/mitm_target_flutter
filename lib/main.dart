import 'dart:convert';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:convert/convert.dart' as convert;
import 'package:flutter/services.dart' show rootBundle;

class MyHttpOverrides extends HttpOverrides {
  var enableCertPinning = true;
  var certs = [
    "certs/httpbin.pem",
    "certs/postman_echo.pem"
  ];

  var useProxy = true;
  var proxy = "192.168.88.100:8888";

  SecurityContext context;
  Future<void> initContext() async {
    if (this.enableCertPinning) {
      this.context = new SecurityContext(withTrustedRoots: false);
      for (var cert in this.certs) {
        ByteData data = await rootBundle.load(cert);
        this.context.setTrustedCertificatesBytes(data.buffer.asUint8List());
      }
    } else {
      this.context = SecurityContext.defaultContext;
    }
  }

  @override
  HttpClient createHttpClient(SecurityContext securityContext) {
    var client = super.createHttpClient(this.context);
    if (this.useProxy && this.proxy != null) {
      client.findProxy = (url) {
        return "PROXY $proxy;";
      };
    }
    return client;
  }
}

void main() async {
  var myHttpOverrides = new MyHttpOverrides();
  HttpOverrides.global = myHttpOverrides;
  WidgetsFlutterBinding.ensureInitialized();
  await myHttpOverrides.initContext();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var urls = [
    "http://httpbin.org/user-agent",
    "https://httpbin.org/user-agent",
    "https://postman-echo.com/get?foo1=bar1&foo2=bar2"
  ];
  List<String> responses = [];

  String _responseToStr(http.Response response) {
    return "URL: ${response.request.url}\n"
        "Code: ${response.statusCode}\n"
        "Reason: ${response.reasonPhrase}\n"
        "Body: ${response.body}";
  }

  void _performRequests() async {
    setState(() {
      this.responses = [];
    });

    List<String> responses = [];
    for (var url in urls) {
      try {
        var response = await http.get(url);
        responses.add(_responseToStr(response));
      } catch (e) {
        responses.add(e.toString());
      }
    }

    setState(() {
      this.responses = responses;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: this.responses.map((str) {
              return Container(margin: EdgeInsets.all(12), child: Text(str));
            }).toList()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _performRequests,
        tooltip: 'Requests',
        child: Icon(Icons.file_download),
      ),
    );
  }
}
