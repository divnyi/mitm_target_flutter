# mitmtarget

Simple network app designed to showcase the MITM attack

## Flutter-specific issues

https://github.com/flutter/flutter/issues/20376
https://github.com/flutter/flutter/issues/26359
